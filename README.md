#  Add New Container

Create the screen class in Container folder, extends Component.

Add mapStateToProps if you need to access the global state.

Add mapDispatchToProps if you need to dispatch some actions.

Use redux connect to connect the state and dispatch to the screen. It will pass the state to mapStateToProps and dispatch to mapDispatchToProps.

You can access the state and dispatch in the screen by using this.props.[state/dispatch name]

You can add the container's initial state in the class's constructor.

Create the styles file inside Container/Styles folder.

Add a render function inside the class to render the screen.

# Navigation

Use the Navigation/AppNavigation.js or create new navigation and reference it as a screen in there.

- StackNavigation is used to navigate from one screen to another screen that stacks on top of it (basically most navigations use this)
- TabNavigation is used to create a navigation with multiple swipeable screens and have a tab indicator to switch to any of those screens

Add new screen definition inside the navigation by giving it a screen name and reference it to the screen you have in Container folder.

The initial screen is defined in the initialRouteName in the navigation option.

To navigate from one screen to another, call the dispatcher to NavigationActions.navigate() and specify the routeName. You can also pass a parameter object in that action.

#  Add New Component

Create the component class in Component folder.

Create the styles file inside Component/Styles folder.

Best practice: define eact props' types in propTypes to give warning on passing wrong prop types.

Add a render function inside the class to render the component.

Component shouldn't have any state, and use the passed props instead to generate its contents.

#  Redux

Create the redux files in Redux folder.

Add the Types and Actions using reduxsauce's createActions function.

Set the initial state in INITIAL_STATE constant.

Add the selectors as a query/parsing/mapping function for the state content.

Add the reducers to change the state. Reducers must be pure functions.

Hookup the Reducers to Types using reduxsauce's createReducer function.

Add the created reducer in Redux/index.js's combineReducers.

#  API Service

Services should be added in Services folder.

Use the Api.js file, change baseURL to your endpoint URL.

Add the corresponding function to send request to endpoint.

Dont't forget to add it in the return object.

#  Sagas

Sagas is used to create async actions.

Add the Sagas inside Sagas folder.

Create the async functions (e.g. API call) inside the file.

Note that function* means it's a generator function.

- yield call is used to call function, and if the function returns a promise, the generator function will be suspended until the promise is resolved or rejected
- yield put is used to dispatch an action, and will not block the function
- yield put.resolve is used to dispatch an action, and will block the function (i.e. if the dispatch returns a promise, the function will be suspended until it's resolved or rejected)


To use the Saga functions, add them to the Sagas/index.js inside the exported generator function root()

- Use takeLatest to take only the latest action of multiple action call
- Use takeEvery to take all the queued actions of multiple action call

It will connect an action type to the Saga function. For example, takeLatest(TodoTypes.TODO_GET, getItems, api) will run getItems when TodoActions.todoGet action is dispatched, passing the api and the payload of that action into the getItems function.

#  APP FLOW EXAMPLE

Example flow for get item request:
- TodoScreen dispatches TodoActions.todoGet() in the constructor
- The dispatch will run the reducer function corresponding to the action. In this case, it will run the get function inside Redux/Todo, and will set the todo state with {fetching:true, error:null}. Since the state is updated, the TodoScreen will display loading text.
- Since the TODO_GET types also registered in Sagas/index.js, it will run the getItems function ins Sagas/Todo, which will call api.getItems and wait for the response to be returned
- If the response is ok, it will dispatch TodoActions.todoGetSuccess with the parsed response of the API call to be passed to the action. It will then call the getSuccess function inside Redux/Todo, and will add the items into the todo state, and set the fetching as false, and error as null. Since the state is updated, the TodoScreen will remove the loading text and re-render the list with updated state.
- If the response has error, it will dispatch TodoActions.todoGetFail with the error from the response passed to the action. It will then call the getFail function inside Redux/Todo, and will set the error in todo state with the passed error, and the fetching as false. Since the state is updated, the TodoScreen will remove the loading text and display the error message.











#  Test
[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg?style=flat)](http://standardjs.com/)

* Standard compliant React Native App Utilizing [Ignite](https://github.com/infinitered/ignite)

## :arrow_up: How to Setup

**Step 1:** git clone this repo:

**Step 2:** cd to the cloned repo:

**Step 3:** Install the Application with `yarn` or `npm i`


## :arrow_forward: How to Run App

1. cd to the repo
2. Run Build for either OS
  * for iOS
    * run `react-native run-ios`
  * for Android
    * Run Genymotion
    * run `react-native run-android`

## :no_entry_sign: Standard Compliant

[![js-standard-style](https://cdn.rawgit.com/feross/standard/master/badge.svg)](https://github.com/feross/standard)
This project adheres to Standard.  Our CI enforces this, so we suggest you enable linting to keep your project compliant during development.

**To Lint on Commit**

This is implemented using [husky](https://github.com/typicode/husky). There is no additional setup needed.

**Bypass Lint**

If you have to bypass lint for a special commit that you will come back and clean (pushing something to a branch etc.) then you can bypass git hooks with adding `--no-verify` to your commit command.

**Understanding Linting Errors**

The linting rules are from JS Standard and React-Standard.  [Regular JS errors can be found with descriptions here](http://eslint.org/docs/rules/), while [React errors and descriptions can be found here](https://github.com/yannickcr/eslint-plugin-react).

## :closed_lock_with_key: Secrets

This project uses [react-native-config](https://github.com/luggit/react-native-config) to expose config variables to your javascript code in React Native. You can store API keys
and other sensitive information in a `.env` file:

```
API_URL=https://myapi.com
GOOGLE_MAPS_API_KEY=abcdefgh
```

and access them from React Native like so:

```
import Secrets from 'react-native-config'

Secrets.API_URL  // 'https://myapi.com'
Secrets.GOOGLE_MAPS_API_KEY  // 'abcdefgh'
```

The `.env` file is ignored by git keeping those secrets out of your repo.

### Get started:
1. Copy .env.example to .env
2. Add your config variables
3. Follow instructions at [https://github.com/luggit/react-native-config#setup](https://github.com/luggit/react-native-config#setup)
4. Done!
