import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts } from '../../Themes/'

export default StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    borderBottomColor: '#AAA',
    borderBottomWidth: 1,
  },
  text: {
    width: Metrics.screenWidth - 55,
    padding:10,
  },
  button: {
    width: 55,
    backgroundColor: '#FFFFFF'
  },
})
