import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Text, Button } from 'react-native'
import styles from './Styles/TodoItemStyles'

export default class TodoItem extends Component {

  static propTypes = {
    id: PropTypes.number,
    text: PropTypes.string,
    delete: PropTypes.func,
  }

  render () {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>{this.props.text}</Text>
        <Button style={styles.button} title={'DELETE'} onPress={() => {this.props.delete(this.props.id)}}/>
      </View>
    )
  }
}
