import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  todoGet: [],
  todoGetSuccess: ["items"],
  todoGetFail: ["error"],

  todoAdd: ["name"],
  todoAddSuccess: ["item"],
  todoAddFail: ["error"],

  todoDel: ["id"],
  todoDelSuccess: ["id"],
  todoDelFail: ["error"],
})

export const TodoTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  items:{},
  fetching:false,
  error:null,
})

/* ------------- Selectors ------------- */

export const TodoSelectors = {
  // getList: state => Object.values(state.todo.items)
}

/* ------------- Reducers ------------- */

// fetch the item
export const get = (state) => state.merge({fetching:true, error:null})

export const getSuccess = (state, { items }) => {
  return state.merge({fetching:false, error:null, items})
}

export const getFail = (state, { error }) => state.merge({fetching:false, error})

// add the item
export const add = (state) => state.merge({fetching:true, error:null})

export const addSuccess = (state, { item }) => {
  const items = state.items.set(item.id, item)
  return state.merge({fetching:false, error:null, items})
}

export const addFail = (state, { error }) => state.merge({fetching:false, error})

// delete the item
export const del = (state) => state.merge({fetching:true, error:null})

export const delSuccess = (state, { id }) => {
  const items = state.items.without(id)
  return state.merge({fetching:false, error:null, items})
}

export const delFail = (state, { error }) => state.merge({fetching:false, error})

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.TODO_GET]: get,
  [Types.TODO_GET_SUCCESS]: getSuccess,
  [Types.TODO_GET_FAIL]: getFail,
  [Types.TODO_ADD]: add,
  [Types.TODO_ADD_SUCCESS]: addSuccess,
  [Types.TODO_ADD_FAIL]: addFail,
  [Types.TODO_DEL]: del,
  [Types.TODO_DEL_SUCCESS]: delSuccess,
  [Types.TODO_DEL_FAIL]: delFail,
})
