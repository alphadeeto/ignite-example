import { call, put } from 'redux-saga/effects'
import TodoActions from '../Redux/Todo'

export function * getItems (api) {
  // make the call to the api
  const response = yield call(api.getItems)

  if (response.ok) {
    const items = {};
    response.data.data.forEach(item => {
      items[item.id] = item;
    });
    yield put(TodoActions.todoGetSuccess(items))
  } else {
    yield put(TodoActions.todoGetFail(response.error))
  }
}


export function * addItem (api, payload) {
  const { name } = payload
  // make the call to the api
  const response = yield call(api.addItem, {name})

  if (response.ok) {
    yield put(TodoActions.todoAddSuccess(response.data))
  } else {
    yield put(TodoActions.todoAddFail(response.error))
  }
}


export function * delItem (api, payload) {
  const { id } = payload
  // make the call to the api
  const response = yield call(api.addItem, {id})

  if (response.ok) {
    yield put(TodoActions.todoDelSuccess(id))
  } else {
    yield put(TodoActions.todoDelFail(response.error))
  }
}