import React, { Component } from 'react'
import { connect } from 'react-redux'
import { ScrollView, Text, Image, View, TextInput, Button, FlatList } from 'react-native'
import TodoItem from '../Components/TodoItem'
import TodoActions from '../Redux/Todo'
import { Images } from '../Themes'

// Styles
import styles from './Styles/TodoScreenStyles'

class TodoScreen extends Component {
  constructor (props) {
    super(props);
    this.state = {
      inputText: '', //container state to contain text input value
    }
    this.props.getItems();
  }

  render () {
    console.log("ITEMS", Object.values(this.props.todo.items))
    const items = Object.values(this.props.todo.items);
    return (
      <View style={styles.mainContainer}>
        <TextInput
          placeholder={"What to do?"}
          onChangeText={(inputText) => this.setState({inputText})} //on text change, set the state. You might want to read the setState function to know more about how to change a container state.
          value={this.state.inputText}
          />
        <Button
          onPress={() => {this.props.addItem(this.state.inputText)}} //this will trigger the addItem function, dispatching the TodoActions.add
          title="ADD"
          />
        {this.props.todo.fetching && <Text>LOADING</Text>}
        {this.props.todo.error && <Text>ERROR: {this.props.todo.error}</Text>}
        <FlatList
          data={items} //use data from todo state
          renderItem={(item) => {
            return <TodoItem 
              id={item.item.id} //item id to be referenced 
              text={item.item.name} //the text to be displayed
              delete={this.props.deleteItem} //function to delete item
              />
          }}
          />
      </View>
    )
  }
}

const mapStateToProps = (state) => ({
  todo: state.todo,
})
  
const mapDispatchToProps = (dispatch) => ({
  addItem: (text) => dispatch(TodoActions.todoAdd(text)),
  deleteItem: (id) => dispatch(TodoActions.todoDel(id)),
  getItems: () => dispatch(TodoActions.todoGet())
})

export default connect(mapStateToProps, mapDispatchToProps)(TodoScreen)
