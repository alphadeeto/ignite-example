import React, { Component } from 'react'
import { connect } from 'react-redux'
import { ScrollView, Text, Image, View, TextInput, TouchableOpacity } from 'react-native'
import { Images, Metrics } from '../Themes'
import { NavigationActions } from 'react-navigation';

// Styles
import styles from './Styles/LaunchScreenStyles'

class LaunchScreen extends Component {
  render () {
    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <ScrollView style={styles.container}>
          <View style={styles.centered}>
            <Image source={Images.launch} style={styles.logo} />
          </View>

          <View style={styles.section} >
            <Image source={Images.ready} />
            <Text style={styles.sectionText}>
              This probably isn't what your app is going to look like. Unless your designer handed you this screen and, in that case, congrats! You're ready to ship. For everyone else, this is where you'll see a live preview of your fully functioning app using Ignite.
            </Text>
            <TouchableOpacity
              onPress={() => (this.props.navigate("TodoScreen", null))}>
              <Text style={{width:Metrics.screenWIdth, backgroundColor: '#FFF', padding:10, textAlign:'center'}}>Go To TODO!</Text>
            </TouchableOpacity>
          </View>

        </ScrollView>
      </View>
    )
  }
}
  
const mapDispatchToProps = (dispatch) => ({
  navigate: (routeName, params) => dispatch(NavigationActions.navigate({
    routeName, params
  }))
})

export default connect(null, mapDispatchToProps)(LaunchScreen)